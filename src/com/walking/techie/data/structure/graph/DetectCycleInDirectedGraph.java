package com.walking.techie.data.structure.graph;

import java.util.Iterator;
import java.util.LinkedList;

//java program to detect cycle in directed graph
public class DetectCycleInDirectedGraph {

  private int v; // no of vertices in a graph
  private LinkedList<Integer> adj[];// array of linked list if adjacency list
  // representation

  public DetectCycleInDirectedGraph(int v) {
    this.v = v;
    adj = new LinkedList[v];
    for (int i = 0; i < v; i++) {
      adj[i] = new LinkedList<Integer>();
    }
  }

  public static void main(String[] args) {
    DetectCycleInDirectedGraph g = new DetectCycleInDirectedGraph(4);

    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 2);
    g.addEdge(2, 0);
    g.addEdge(2, 3);
    g.addEdge(3, 3);

    if (g.isCyclic()) {
      System.out.println("Graph contain cycle.");
    } else {
      System.out.println("Graph doesn't contain cycle.");
    }
  }

  // addEdge method to add edge between vertex i and vertex j
  public void addEdge(int i, int j) {
    adj[i].add(j);
  }

  public boolean isCyclic() {
    // mark all the vertices not visited
    boolean visited[] = new boolean[this.v];
    boolean recStack[] = new boolean[this.v];
    for (int i = 0; i < this.v; i++) {
      if (isCyclicUtil(i, visited, recStack)) {
        return true;
      }
    }
    return false;
  }

  private boolean isCyclicUtil(int i, boolean[] visited, boolean[] recStack) {
    //mark current node as visited and part of recursion stack
    visited[i] = true;
    recStack[i] = true;
    Iterator<Integer> itr = adj[i].iterator();
    while (itr.hasNext()) {
      int n = itr.next();
      if (!visited[n] && isCyclicUtil(n, visited, recStack)) {
        return true;
      } else if (recStack[n]) {
        return true;
      }
    }
    recStack[i] = false; // remove the vertex from recursion stack
    return false;
  }
}